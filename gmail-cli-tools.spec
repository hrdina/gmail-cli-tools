Name: gmail-cli-tools
Version: 1
Release: 1%{?dist}
Summary: Tool to manage xoauth2 google authentication and gmail filters.

License: MIT
URL: https://gitlab.com/hrdina/gmail-cli-tools/
Source: gmail_cli_tools-%{version}.tar.gz

BuildArch: noarch
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-wheel


%description
Tool to manage xoauth2 google authentication and gmail filters.


%prep
%autosetup -n gmail_cli_tools-%{version}


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel


%install
%pyproject_install


%files
%license LICENSE
%{_bindir}/*
%{python3_sitelib}/*


%changelog
%autochangelog

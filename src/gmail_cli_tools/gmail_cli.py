#!/usr/bin/env python3

import argparse
import httplib2
import configparser
import json
import os
import sys

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_httplib2 import AuthorizedHttp
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient import discovery


def labelIdToName(labels, ids):
    ret = []
    for label in labels:
        ret.append(ids[label]['name'])
    return ret


def labelNameToId(labels, names):
    ret = []
    for label in labels:
        ret.append(names[label]['id'])
    return ret


def sortFilters(item):
    return item['labels']


def compareFilter(a, b):
    if a['match'] != b['match']:
        return False

    if a['not_match'] != b['not_match']:
        return False

    if a['labels'].sort() != b['labels'].sort():
        return False

    if a['not_labels'].sort() != b['not_labels'].sort():
        return False

    return True


def filterExists(filtr, filters):
    for f in filters:
        if compareFilter(f, filtr):
            return True

    return False


def split_list(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i+n]


class GmailCliTools:
    def __init__(self):
        self._config = None
        self._creds = None
        self._service = None
        self._filter_file = None

        self._args = None

    @property
    def config(self):
        if self._config:
            return self._config

        configfile = self._args.config
        if not configfile:
            configfile = os.path.join(os.path.expanduser('~'), '.gmail-cli.conf')

        if not os.path.exists(configfile):
            print('config file {} does not exist'.format(configfile))
            sys.exit(1)

        config = configparser.ConfigParser()
        config.read(configfile)

        if self.service_name not in config:
            print('there is no config for {} in configfile {}'.format(self.service_name, configfile))
            sys.exit(1)

        self._config = config[self.service_name]

        return self._config

    @property
    def creds(self):
        if not self._creds:
            creds_file = os.path.expanduser(self.config['creds'])
            self._creds = Credentials.from_authorized_user_file(creds_file)

        if self._creds.expired:
            self._creds.refresh(Request())

        return self._creds

    @property
    def service(self):
        if not self._service:
            http = httplib2.Http()
            http.redirect_codes = http.redirect_codes - {308}

            http = AuthorizedHttp(self.creds, http)

            self._service = discovery.build('gmail', 'v1', http=http)

        return self._service

    @property
    def filter_file(self):
        if not self._filter_file:
            filter_file = self.config.get('filters', None)

            if filter_file is None:
                print('missing filters for {} in configfile'.format(self.service_name))
                sys.exit(1)

            self._filter_file = os.path.expanduser(filter_file)

        return self._filter_file

    @property
    def action(self):
        return self._args.action

    @property
    def dry_run(self):
        return self._args.dry_run

    @property
    def service_name(self):
        return self._args.name

    @property
    def filter_label(self):
        return self._args.label

    def get_labels(self):
        result = self.service.users().labels().list(userId='me').execute()
        labels = result.get('labels', [])

        labelsByName = {}
        labelsById = {}

        for label in labels:
            labelsByName[label['name']] = label
            labelsById[label['id']] = label

        return labelsById, labelsByName

    def get_filters(self, wrap=False):
        result = self.service.users().settings().filters().list(userId='me').execute()
        filters = result.get('filter', [])

        ids, names = self.get_labels()

        ret = []

        for filtr in filters:
            if 'action' in filtr:
                addLabels = filtr['action'].get('addLabelIds', [])
                delLabels = filtr['action'].get('removeLabelIds', [])
            else:
                addLabels = []
                delLabels = []

            item = {
                'labels': labelIdToName(addLabels, ids),
                'not_labels': labelIdToName(delLabels, ids),
                'match': filtr['criteria'].get('query', ''),
                'not_match': filtr['criteria'].get('negatedQuery', ''),
            }

            if wrap:
                item = {
                    'id': filtr['id'],
                    'data': item,
                }

            ret.append(item)

        if not wrap:
            ret.sort(key=sortFilters)

        return ret

    def set_filters(self, filters):
        curFilters = self.get_filters()
        ids, names = self.get_labels()

        count = 0

        for filtr in filters:
            if filterExists(filtr, curFilters):
                continue

            newFilter = {
                'action': {
                    'addLabelIds': labelNameToId(filtr['labels'], names),
                    'removeLabelIds': labelNameToId(filtr['not_labels'], names),
                },
                'criteria': {
                    'query': filtr['match'],
                    'negatedQuery': filtr['not_match'],
                },
            }

            if not self.dry_run:
                self.service.users().settings().filters().create(
                    userId='me',
                    body=newFilter,
                ).execute()

            count += 1

        print('filters added: {0}'.format(count))

    def prune_filters(self, filters):
        curFilters = self.get_filters(wrap=True)

        count = 0

        for filtr in curFilters:
            if filterExists(filtr['data'], filters):
                continue

            if not self.dry_run:
                self.service.users().settings().filters().delete(
                    userId='me',
                    id=filtr['id'],
                ).execute()

            count += 1

        print('filters removed: {0}'.format(count))

    def get_page(self, query, nextPageToken):
        return self.service.users().messages().list(
                    userId='me',
                    maxResults=500,
                    q=query,
                    pageToken=nextPageToken,
                ).execute()

    def get_mails(self, query):
        mailIds = []
        nextPageToken = None

        while True:
            print(f'Loading messages: {len(mailIds)}', end='\r')

            result = self.get_page(query, nextPageToken)

            if result['resultSizeEstimate'] <= 0:
                break

            mailIds.extend(map(lambda msgs: msgs['id'], result['messages']))

            if 'nextPageToken' not in result:
                break
            nextPageToken = result['nextPageToken']

        print(end='\x1b[2K')
        print(f'Loaded messages: {len(mailIds)}')
        return mailIds

    def apply_filters(self, filters):
        ids, names = self.get_labels()

        count = 0

        for filtr in filters:
            if self.filter_label not in filtr['labels']:
                continue

            query = filtr['match']
            if filtr['not_match'] != "":
                query += f' -({filtr["not_match"]})'

            mailIds = self.get_mails(query)

            for ids in split_list(mailIds, 1000):
                data = {
                    'ids': ids,
                    'addLabelIds': labelNameToId(filtr['labels'], names),
                    'removeLabelIds': labelNameToId(filtr['not_labels'], names),
                }

                if not self.dry_run:
                    self.service.users().messages().batchModify(
                            userId='me',
                            body=data,
                    ).execute()

                count += len(ids)

        print(f'label applied to {count} messages')

    def archive_messages(self):
        from datetime import date

        count = 0
        query = f'label:{self._args.cur_label}'
        ids, names = self.get_labels()

        if self._args.before:
            before = date.fromisoformat(self._args.before)
            query += f' before:{before}'

        mailIds = self.get_mails(query)

        if self._args.keep:
            s = slice(self._args.keep, len(mailIds))
            mailIds = mailIds[s]

        add_labels = []
        if self._args.new_label:
            add_labels.append(self._args.new_label)

        remove_labels = [self._args.cur_label]

        for ids in split_list(mailIds, 1000):
            print(f'Archiving messages: {count}', end='\r')

            data = {
                'ids': ids,
                'addLabelIds': labelNameToId(add_labels, names),
                'removeLabelIds': labelNameToId(remove_labels, names),
            }

            if not self.dry_run:
                self.service.users().messages().batchModify(
                        userId='me',
                        body=data,
                ).execute()

            count += len(ids)

        print(end='\x1b[2K')
        print(f'Archived messages: {count}')

    def action_auth(self):
        from importlib.resources import files

        secret_file = files('gmail_cli_tools').joinpath('secret.ini')

        config = configparser.ConfigParser()
        config.read(secret_file.absolute())

        if config['secret']['client_type'] == 'internal':
            scope = [
                'https://mail.google.com/',
                'https://www.googleapis.com/auth/gmail.labels',
                'https://www.googleapis.com/auth/gmail.settings.basic',
            ]
        else:
            scope = [
                'https://mail.google.com/',
            ]

        flow = None

        secret = {
            "installed": {
                "client_id": config['secret']['client_id'],
                "project_id": "gmail-cli-tools",
                "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                "token_uri": "https://www.googleapis.com/oauth2/v3/token",
                "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                "client_secret": config['secret']['client_secret'],
                "redirect_uris": ["https://mail.google.com/"],
            }
        }

        creds_file = os.path.expanduser(self.config['creds'])

        flow = InstalledAppFlow.from_client_config(secret, scope)

        flow.run_local_server(port=0)

        with open(creds_file, 'w') as file:
            file.write(flow.credentials.to_json())

    def action_token(self):
        print(self.creds.token)

    def filter_fetch(self):
        filters = self.get_filters()

        with open(self.filter_file, 'w') as file:
            json.dump(filters, file, indent=2)

    def filter_push(self):
        with open(self.filter_file) as file:
            filters = json.load(file)

        self.set_filters(filters)

    def filter_prune(self):
        with open(self.filter_file) as file:
            filters = json.load(file)

        self.prune_filters(filters)

    def filter_apply(self):
        with open(self.filter_file) as file:
            filters = json.load(file)

        self.apply_filters(filters)

    def filter_archive(self):
        self.archive_messages()

    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--config',
                            help='path to configuration file (default is ~/.gmail-cli.conf)')
        parser.add_argument('-n', '--dry-run', action='store_true', default=False,
                            help='do not make any API calls to gmail')

        subparsers = parser.add_subparsers(metavar='ACTION', required=True)
        subparsers.required = True

        authparser = subparsers.add_parser('auth', help='run authentication')
        authparser.set_defaults(func=self.action_auth)
        authparser.add_argument('name', help='service name from configuration file')

        tokenparser = subparsers.add_parser('token', help='get token')
        tokenparser.set_defaults(func=self.action_token)
        tokenparser.add_argument('name', help='service name from configuration file')

        filterparser = subparsers.add_parser('filter', help='manage gmail filters')
        filterparser.add_argument('name', help='service name from configuration file')

        filtersubparsers = filterparser.add_subparsers(metavar='OPERATION', required=True)

        fetchparser = filtersubparsers.add_parser('fetch', help='fetch filters from gmail')
        fetchparser.set_defaults(func=self.filter_fetch)

        pushparser = filtersubparsers.add_parser('push', help='push filters to gmail')
        pushparser.set_defaults(func=self.filter_push)

        pruneparser = filtersubparsers.add_parser('prune', help='prune unused filters from gmail')
        pruneparser.set_defaults(func=self.filter_prune)

        applyparser = filtersubparsers.add_parser('apply', help='apply filter to all mails')
        applyparser.set_defaults(func=self.filter_apply)
        applyparser.add_argument('label', help='name of label to apply')

        archiveparser = filtersubparsers.add_parser('archive', help='move mails to different label')
        archiveparser.set_defaults(func=self.filter_archive)
        archiveparser.add_argument('cur_label', help='label to search for')
        archiveparser.add_argument('new_label', nargs='?', default=None,
                                   help='new label applied to archived messages')

        archivegroup = archiveparser.add_mutually_exclusive_group(required=True)
        archivegroup.add_argument('--keep', type=int, help='number of messages to keep')
        archivegroup.add_argument('--before', help='archive messages before date in ISO format')

        self._args = parser.parse_args()

    def run(self):
        self.parse_args()

        self._args.func()

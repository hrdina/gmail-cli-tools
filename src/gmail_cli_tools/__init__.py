#!/usr/bin/env python3

from .gmail_cli import GmailCliTools

def run():
    app = GmailCliTools()
    app.run()
